package hwo2014

import org.json4s._
import org.json4s.DefaultFormats
import java.net.Socket
import java.io.{BufferedReader, InputStreamReader, OutputStreamWriter, PrintWriter}
import org.json4s.native.Serialization
import scala.annotation.tailrec

object NoobBot extends App {
  args.toList match {
    case hostName :: port :: botName :: botKey :: _ =>
      new NoobBot(hostName, Integer.parseInt(port), botName, botKey)
    case _ => println("args missing")
  }
}

class NoobBot(host: String, port: Int, botName: String, botKey: String) {
  implicit val formats = new DefaultFormats{}
  val socket = new Socket(host, port)
  val writer = new PrintWriter(new OutputStreamWriter(socket.getOutputStream, "UTF8"))
  val reader = new BufferedReader(new InputStreamReader(socket.getInputStream, "UTF8"))
  send(MsgWrapper("join", Join(botName, botKey)))
  play

  var track: Track = null

  var velocity: Double = 0.0
  var prevPosition: Double = 0.0
  var prevTimestamp: Long = System.currentTimeMillis

  @tailrec private def play() {
    val line = reader.readLine()
    if (line != null) {
      Serialization.read[MsgWrapper](line) match {
        case MsgWrapper("carPositions", data) =>
          val carPosition = data.extract[List[CarPosition]].head
          print(carPosition + ";\t")

          val currentPosition = carPosition.piecePosition.inPieceDistance
          velocity = (currentPosition - prevPosition) / (System.currentTimeMillis - prevTimestamp)

          val nextAngle = track.getNextPiece(carPosition.piecePosition.pieceIndex).getAbsAngle
          val currentAngle = track.getPiece(carPosition.piecePosition.pieceIndex).getAbsAngle
          if (currentAngle == 0 && nextAngle == 0) {
            sendThrottle(1.0)
          } else if (nextAngle >= 45) {
            if (velocity > 0.000000000026) sendThrottle(0.2) else sendThrottle(0.5)
          } else {
            sendThrottle(0.8)
          }
          println(f"$velocity%1.15f , angle angle " + track.getPiece(carPosition.piecePosition.pieceIndex))
        case MsgWrapper("gameInit", data) =>
          println("gameInit")
          track = data.extract[GameInit].race.track
          println(s"pieces ${track.pieces}")
        case MsgWrapper(msgType, data) =>
          println("Received: " + msgType)
      }

      sendThrottle(1)
      play
    }
  }

  def sendThrottle(throttle: Double) {
    send(MsgWrapper("throttle", throttle))
  }

  def send(msg: MsgWrapper) {
    writer.println(Serialization.write(msg))
    writer.flush
  }

}

case class GameInit(race: Race)
case class Race(track: Track)
case class Track(pieces: List[Piece]) {
  def getNextPiece(i: Integer) = {
    pieces.lift(i + 1) match {
      case Some(x) => x
      case None => pieces(0)
    }
  }
  def getPiece(i: Integer) = pieces(i)
}
case class Piece(length: Option[Double], switch: Option[Boolean], radius: Option[Double], angle: Option[Double]) {
  def getAbsAngle = angle.getOrElse(0.0).abs
}

case class CarPosition(id: Id, angle: Double, piecePosition: PiecePosition)
case class Id(name:String, color: String)
case class PiecePosition(pieceIndex: Int, inPieceDistance: Double, lane: Lane)
case class Lane(startLaneIndex: Int, endLaneIndex: Int)

case class Join(name: String, key: String)
case class MsgWrapper(msgType: String, data: JValue) {
}
object MsgWrapper {
  implicit val formats = new DefaultFormats{}

  def apply(msgType: String, data: Any): MsgWrapper = {
    MsgWrapper(msgType, Extraction.decompose(data))
  }
}
